<?php

/**
 * Created by PhpStorm.
 * User: IstoD
 * Date: 09.08.2017
 * Time: 13:21
 */

class Analytic
{
    protected $triangles;

    /**
     * Analytic constructor.
     * @param array $triangles Triangle
     */
    public function __construct(array $triangles)
    {
        $this->triangles = $triangles;
    }

    public function midPerimeter()
    {
        $per = [];

        foreach ($this->triangles as $key => $triangle) {
            $per[] = $triangle -> Perimeter();
        }

        return array_sum ($per)/count($per);
    }

    public function LargestA()
    {
        $largestA = [];

        foreach ($this->triangles as $key => $triangle) {
            $largestA[] = $triangle -> getA();
        }

        return max($largestA);
    }

    public function LargestSquare()
    {
        $halfperimeter = [];
        $sourceA = [];
        $sourceB = [];
        $sourceC = [];
        $geronmax = [];

        foreach ($this->triangles as $key => $triangle) {
            $halfperimeter [] = $triangle -> Perimeter()/2;
            $sourceA [] = $triangle -> getA();
            $sourceB [] = $triangle -> getB();
            $sourceC [] = $triangle -> getC();
            $geron1 = ($halfperimeter[$key]);
            $geron2 = ($halfperimeter[$key]-$sourceA[$key]);
            $geron3 = ($halfperimeter[$key]-$sourceB[$key]);
            $geron4 = ($halfperimeter[$key]-$sourceC[$key]);
            $geron5 = ($geron1*$geron2*$geron3*$geron4);
            $geron6 = sqrt($geron5);
            $geronmax [] = $geron6;
            //var_dump($geron6);
        }
        $max = -1*PHP_INT_MAX;
        foreach($geronmax as $value) {
            if ($max < $value) {
                $max = $value;
            }
        }
        return round($max, 2);
    }

}



?>