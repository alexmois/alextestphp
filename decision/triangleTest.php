<?php

/**
 * Created by Alexander Moiseykin
 * master@cifr.us
 * Kyiv, Ukraine
 *
 * Date: 20.07.17
 * Time: 17:56
 */
require_once 'Triangle.php';
require_once 'Analytic.php';

//  Решение задачи 1.3 Выполнение методов класса________________________________________________________________________

$trianglearray = [];

for ($i = 0; $i < 10; $i++)
{
    $trianglearray [] = $triangle = new Triangle();
}

foreach ($trianglearray as $triangle)

{
   echo $triangle->Perimeter();
   echo " - Периметр треугольника <br/>";
   if ($triangle->isExist() == 1)
   {
       echo "Треугольник существует <br/>";
   }
   if ($triangle->isRectangular() == 1)
   {
       echo "Треугольник прямоугольный <br/>";
   }
   if ($triangle->isIsosceles() == 1)
   {
       echo "Треугольник равнобедренный <br/>";
   }
   echo "<br/>";
}
echo "<hr/>";

$newanal = new Analytic($trianglearray);



    /**
     * @var $newanal Analytic
     */
    echo $newanal->Midperimeter();
    echo " - Среднее значение периметра <br/>";
    echo $newanal->LargestA();
    echo " - Самое большое значение стороны А <br/> ";
    $largestSquare = $newanal->LargestSquare();
    if($largestSquare <= 0 )
        echo "Не возможно посчитать площадь, значение меньше или равно нулю";
    elseif(is_nan ($largestSquare))
        echo "Не возможно посчитать площадь, значение не определено";
    else
        echo "$largestSquare - Самое большое значение площади";




/**Решение задачи 2.1 Среднее арифметическое из периметров______________________________________________________________


$per = [];

for ($i = 0; $i < 10; $i++)
{
    $triangle = new Triangle();
    $per [] = $triangle -> Perimeter();
}

echo array_sum ($per)/count($per);
*/

/**Решение задачи 2.2 Самая большая сторона А___________________________________________________________________________

$largestA = [];

for ($i = 0; $i < 10; $i++)
{
    $triangle = new Triangle();
    $largestA [] = $triangle -> getA();
}

echo max($largestA);

*/
/**

//Решение задачи 2.3 Найти самое большое значение площади_______________________________________________________________

//Формула Герона S=sqrt{p*(p-a)*(p-b)*(p-c)} *S-площадь p-полупериметр abc-стороны

$halfperimeter = [];
$sourceA = [];
$sourceB = [];
$sourceC = [];
$geronmax = [];
// В этом цикле вычисляем полупериметр и каждую сторону
// И считаем по формуле Герона
for ($i = 0; $i < 10; $i++)
{
    $triangle = new Triangle();
    $halfperimeter [] = $triangle -> Perimeter()/2;
    $sourceA [] = $triangle -> getA();
    $sourceB [] = $triangle -> getB();
    $sourceC [] = $triangle -> getC();
    $geron1 = ($halfperimeter[$i]);
    $geron2 = ($halfperimeter[$i]-$sourceA[$i]);
    $geron3 = ($halfperimeter[$i]-$sourceB[$i]);
    $geron4 = ($halfperimeter[$i]-$sourceC[$i]);
    $geron5 = ($geron1*$geron2*$geron3*$geron4);
    $geron6 = sqrt ($geron5);
    $geronmax [] = $geron6;
}
echo max(($geronmax));

// Просмотр каждой переменной
//print_r ($sourceA);
//print_r ($sourceB);
//print_r ($sourceC);
//print_r ($halfperimeter);_____________________________________________________________________________________________
 */

?>





