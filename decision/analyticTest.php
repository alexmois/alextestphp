<?php
/**
 * Created by Alexander Moiseykin
 * master@cifr.us
 * Kyiv, Ukraine
 *
 * Date: 20.07.17
 * Time: 18:12
 */

require_once 'Analytic.php';

$analtestarray [] = $newanal = new Analytic();

foreach ($analtestarray as $newanal) {
    echo $newanal->Midperimeter();
    echo " - Среднее значение периметра <br/>";
    echo $newanal->LargestA();
    echo " - Самое большое значение стороны А <br/> ";
    if($newanal->LargestSquare() <= 0 )
        echo "Не возможно посчитать площадь, значение меньше или равно нулю";
    elseif(is_nan ($newanal->LargestSquare()))
        echo "Не возможно посчитать площадь, значение не определено";
    else
        echo $newanal->LargestSquare();

}

?>