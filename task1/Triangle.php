
<?php
/**
 * Created by PhpStorm.
 * User: IstoD
 * Date: 27.07.2017
 * Time: 13:13
 */
require 'TriangleAbstract.php';

class Triangle extends TriangleAbstract
{
    public $A;
    public $B;
    public $C;

    public function isExist() : bool
    {
        if (($this->A+$this->B > $this->C)
            ||
            ($this->B+$this->C > $this->A)) {
            return true;
        }
        return false;
    }
    public function perimeter() : int
    {
        return ($this->A + $this->B + $this->C);
    }
    public function isRectangular() : bool
    {
        if (($this->A*$this->A+$this->B*$this->B == $this->C*$this->C)
            ||
            ($this->C*$this->C+$this->B*$this->B == $this->A*$this->A)
            ||
            ($this->A*$this->A+$this->C*$this->C == $this->B*$this->B))
        {
            return true;
        }
        return false;
    }
    public function isIsosceles() : bool
    {
        if (($this->A == $this->B)
            ||
            ($this->B == $this->C)
            ||
            ($this->C == $this->A))
        {
            return true;
        }
        return false;
    }
}

?>
<h2>

</h2>
<?php
/*** конец строки ***/
