<?php

/**
 * Created by Alexander Moiseykin
 * master@cifr.us
 * Kyiv, Ukraine
 *
 * Date: 20.07.17
 * Time: 17:48
 */
abstract class TriangleAbstract
{
    protected $A;
    protected $B;
    protected $C;

    /**
     * TriangleAbstract constructor.
     */
    final public function __construct()

    {
        $this->A = random_int(3, 10);
        $this->B = random_int(4, 12);
        $this->C = random_int(3, 17);
    }

    /**
     * Проеверка треугольника на обязательное существование
     *
     * @return bool
     */

    abstract public function isExist();

    /**
     * Вывод периметра треугольника
     *
     * @return double
     */
    abstract public function perimeter();

    /**
     * Проверка, является ли прямоугольным
     *
     * @return bool
     */
    abstract public function isRectangular();

    /**
     * Проверка, является ли равнобедренным
     *
     * @return bool
     */
    abstract public function isIsosceles();

    /**
     * Сторона А
     *
     * @return int
     */
    public function getA() : int
    {
        return $this->A;
    }

    /**
     * Сторона В
     *
     * @return int
     */
    public function getB() : int
    {
        return $this->B;
    }

    /**
     * Сторона С
     *
     * @return int
     */
    public function getC() : int
    {
        return $this->C;
    }

}

?>

